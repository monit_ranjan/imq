import sys
sys.path.append('../')
import mysql.connector
from constant import *
from defined_exception import *
from string_literal import *

class Database:
    def create_db_Connection(self):
        connection = mysql.connector.connect(
            host=DBHOST,
            user=DBUSER,
            password=DBPASSWORD,
            database=DBNAME
        )
        return connection
    
    def execute_query(self,query,connection, value = ""):
        try:
            cursor = connection.cursor()
            if(value == ""):
                cursor.execute(query)
            else:
                cursor.execute(query,value)
                
            if('select' in query.lower()):
                return cursor.fetchall()
            elif('insert' in query.lower()):
                return cursor.lastrowid
        except mysql.connector.Error as e:  
            raise mysql_connector_error
    
    def create_user_table(self, connection):
        try:
            query = "create table if not exists users (user_id INT AUTO_INCREMENT, user_name varchar(20), Primary key(user_id), UNIQUE(user_name))"
            self.execute_query(query,connection)
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
    
    def check_user_existence(self,user_name,connection):
        query = "select user_name1 from users where user_name = %s"
        try:
            users=self.execute_query(query,connection, (user_name,))
            if(len(users)>0):
                return True
            return False
        except mysql_connector_error as error:
            print(unable_to_execute_query)
            return False
        
    def populate_user_table(self,user_name,connection):
        query = "insert into users (user_name) values (%s)"
        try:
            user_name_status=self.check_user_existence(user_name,connection)
            if(not user_name_status):
                self.execute_query(query,connection, (user_name,))
                connection.commit()
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
    
    def check_user(self, connection,user_name):
        try:
            query = "Select user_id from users where user_name=%s"
            user = self.execute_query(query,connection,(user_name,))
            if(len(user)==1):
                return True
            return False
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
        
    def create_subscription_status_table(self,connection):
        try:
            query = "create table if not exists subscription_status(user_name varchar(20),time_stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,topic_id INT,Primary key(user_name,topic_id),FOREIGN KEY (topic_id) REFERENCES topics (topic_id),FOREIGN KEY (user_name) REFERENCES users (user_name))"
            self.execute_query(query,connection)
            connection.commit()
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
        
    def populate_subscription_status_table(self,user_name,topic_id,connection):
        query = "insert into subscription_status(topic_id,user_name) values (%s,%s)"
        try:
            self.execute_query(query,connection, (topic_id,user_name))
            connection.commit()
            return True,True
        except mysql.connector.IntegrityError as err:
            query="select time_stamp from subscription_status where user_name=%s and topic_id=%s"
            time_stamp = self.execute_query(query,connection, (user_name,topic_id))[0][0]
            query="update subscription_status set time_stamp=now() where user_name=%s"
            self.execute_query(query,connection, (user_name,))
            connection.commit()
            return True,False,time_stamp
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False,False
        
    def check_topic_existence(self,topic_name,connection):
        query = " select topic_name from topics where topic_name = %s"
        try:
            topics=self.execute_query(query,connection, (topic_name,))
            if(len(topics)>0):
                return True
            return False
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
    
    def create_topics_table(self, connection):
        try:
            query = "create table if not exists topics (topic_id INT AUTO_INCREMENT, topic_name varchar(20), Primary key(topic_id), UNIQUE(topic_name))"
            self.execute_query(query,connection)
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
        
    def populate_topics_table(self,topic_name,connection):
        query = "insert into topics (topic_name) values (%s)"
        try:
            topic_name_status=self.check_topic_existence(topic_name,connection)
            if(not topic_name_status):
                self.execute_query(query,connection, (topic_name,))
                connection.commit()
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
    
    def view_topic(self, connection):
        try:
            query = "Select * from topics"
            topics_list = self.execute_query(query,connection)
            topics_list.sort(key = lambda x: x[0])
            return True,topics_list
        except mysql_empty_data_error:
            print(unable_to_fetch_data)
            return False,"No topics exist"
                
    def view_messages(self, connection,topic_id):
        try:
            query = "Select * from messages where topic_id=%s"
            messages_list = self.execute_query(query,connection,(topic_id,))
            return True,messages_list
        except mysql_empty_data_error:
            print(unable_to_fetch_data)
            return False,"no messages exist"
    
    def create_messages_table(self, connection):
        try:
            query = "create table if not exists messages (message_id INT AUTO_INCREMENT,time_stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, message varchar(100),topic_id INT,user_name varchar(20),Primary key(message_id),FOREIGN KEY (topic_id) REFERENCES topics (topic_id),FOREIGN KEY (user_name) REFERENCES users (user_name))"
            self.execute_query(query,connection)
            connection.commit()
            return True
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False
    
    def post_messages(self,connection,topic_id,message,user_name):
        query = "insert into messages(message,topic_id,user_name) values (%s,%s,%s)"
        try:
            message_id=self.execute_query(query,connection, (message,topic_id,user_name))
            connection.commit()
            query="select time_stamp from messages where message_id=%s"
            time_stamp=self.execute_query(query,connection, (message_id,))
            return True,time_stamp[0][0]
        except mysql_connector_error:
            print(unable_to_execute_query)
            return False,0
        
        