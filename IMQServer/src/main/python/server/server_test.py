import sys
import unittest
sys.path.append('../')
from server import Server
import warnings
class ServerTest(unittest.TestCase):
    
    def setUp(self):
        self.server_object = Server()
    
    def supress_resource_warning(self):
        warnings.filterwarnings(action="ignore",
                                message="unclosed",
                                category=ResourceWarning)
    
    def test_server_socket(self):
        self.supress_resource_warning()
        self.assertIsNotNone(self.server_object.server_socket())
        
    # @wrap_case('', 12345, 0)
    def test_start_connection(self):
        self.assertIsNotNone(self.server_object.start_connection())
    
    def test_db_connection(self):
        self.assertIsNotNone(self.server_object.create_db_connection())
        
if __name__ == '__main__':
    unittest.main()
    
    
