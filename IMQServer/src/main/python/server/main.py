from defined_exception import *
from constant import HOST, PORT
from server import Server
from string_literal import *

if __name__ == "__main__":
    try:
        server_connection = Server()
        server_socket = server_connection.start_connection()
        server_connection.run(server_socket)
    except OSError:
        raise multi_socket_binded_error