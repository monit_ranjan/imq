class error(Exception):
    pass

class multi_socket_binded_error(error):
    pass

class unable_to_execute_query(error):
    pass

class unable_to_fetch_data(error):
    pass
    
class OSError(multi_socket_binded_error):
    pass

class mysql_connector_error(unable_to_execute_query):
    pass

class mysql_empty_data_error(unable_to_fetch_data):
    pass
