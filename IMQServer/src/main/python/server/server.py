import sys
sys.path.append('../')
import time
from constant import FORMAT, HOST, PORT, TIMEFORMAT
from Database import *
import socket
import csv
from _thread import *
from string_literal import *
from protocol.parser import Parser
from protocol.response import Response

class Server:
    thread_Count = 0
    message_queue=dict()
    
    def __init__(self):
        start_new_thread(self.remove_expired_messages,())
    
    def server_socket(self):
        server_socket = socket.socket()
        return server_socket
    
    def start_connection(self):
        server_socket=socket.socket()
        try:
            server_socket.bind((HOST,PORT))
        except socket.error:
            print(unable_to_start_server)
        return server_socket
        
    def create_db_connection(self):
        database = Database()
        connection = database.create_db_Connection()
        return database,connection
    
    def remove_expired_messages(self):
        while True:
            for topic in Server.message_queue:
                for index in range(len(Server.message_queue[topic])):
                    time_stamp=time.localtime()
                    message_timestamp=time.strptime(Server.message_queue[topic][index][1],TIMEFORMAT)
                    time_difference=time.mktime(time_stamp)-time.mktime(message_timestamp)
                    if(time_difference>ONEHOUR):
                        Server.message_queue[topic].pop(index)
     
    def get_messages_from_topic(self,topic_name,time_stamp):
        messages_list=list()
        time_stamp=time_stamp.strftime(TIMEFORMAT)
        for record in Server.message_queue[topic_name][::-1]:
            if record[1]>time_stamp:
                messages_list.append(record)
            else:
                break
        return messages_list[::-1]
    
    def subscribe_topic(self,topic_name,user_name):
        messages=list()
        database,connection=self.create_db_connection()
        table_status=database.create_subscription_status_table(connection)
        if(table_status==True):
            insertion_status=database.populate_subscription_status_table(user_name,topic_name,connection)
            if(insertion_status[0] and insertion_status[1]):
                messages=[[subscription_msg,'-']]
            elif(insertion_status[0] and not insertion_status[1] and topic_name in Server.message_queue.keys()):
                messages=self.get_messages_from_topic(topic_name,insertion_status[2])
            elif not insertion_status[0]:
                return False,messages
            return True,messages
        return table_status

    def create_and_populate_message_status_tables(self,topic_name):
        database,connection=self.create_db_connection()
        table_status=database.create_message_status_table(connection)
        if(table_status==True):
            insertion_status=database.populate_message_status_table(topic_name,connection)
            return insertion_status
        return table_status
    
    def get_topics(self):
        database,connection=self.create_db_connection()
        status,topics_list=database.view_topic(connection)
        return status,topics_list
    
    def get_messages(self,topic_name):
        database,connection=self.create_db_connection()
        status,messages_list=database.view_messages(connection,topic_name)
        return status,messages_list
    
    def populate_message_to_queue(self,time_stamp,topic_name,message):
        time_stamp=time_stamp.strftime(TIMEFORMAT)
        if(topic_name not in Server.message_queue.keys()):
            Server.message_queue[topic_name]=[(message,time_stamp)]
        else:
            Server.message_queue[topic_name].append((message,time_stamp))
        
    def create_and_populate_messages_tables(self,topic_name,message,user_name):
        database,connection=self.create_db_connection()
        table_status=database.create_messages_table(connection)
        if(table_status==True):
            insertion_status,time_stamp=database.post_messages(connection,topic_name,message,user_name)
            if(insertion_status):
                self.populate_message_to_queue(time_stamp,topic_name,message)
            return insertion_status
        return table_status
    
    def create_and_populate_topics_tables(self,topic_name):
        database,connection=self.create_db_connection()
        table_status=database.create_topics_table(connection)
        if(table_status==True):
            insertion_status=database.populate_topics_table(topic_name,connection)
            return insertion_status
        return table_status
    
    def create_and_populate_user_table(self,user_name):
        database,connection=self.create_db_connection()
        table_status=database.create_user_table(connection)
        if(table_status==True):
            insertion_status=database.populate_user_table(user_name,connection)
            return insertion_status
        return table_status
    
    def send_response_to_client(self,data,connection):
        response=Response(data)
        encoded_response=Parser.get_encoded_json(response)
        connection.sendall(str.encode(encoded_response))
    
    def send_response_with_status_to_client(self,status,data,connection):
        response=Response(status,data)
        encoded_response=Parser.get_encoded_json(response)
        connection.sendall(str.encode(encoded_response))
        
    def verify_admin_credentials(self,user_name,password):
        with open('admin_credentials.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count != 0:
                    admin_user,admin_password=row[0],row[1]
                    if(admin_user==user_name and admin_password==password):
                        return True
                line_count += 1
            return False
    
    def verify_user_name(self,user_name):
        database,connection=self.create_db_connection()
        status=database.check_user(connection,user_name)
        return status
              
    def process_client_request(self,connection,address):
        connection.send(str.encode(welcome_msg))
        while True:
            data = connection.recv(2048)
            if(not data):
                continue
            decoded_request=Parser.get_decoded_json(data.decode(FORMAT))
            if(decoded_request['request_type']=='user_check'):
                create_user_status=self.verify_user_name(decoded_request['user_name'])
                self.send_response_to_client(create_user_status,connection)
            elif(decoded_request['request_type']=='authenticate_admin'):
                validate_admin_status=self.verify_admin_credentials(decoded_request['user_name'],decoded_request['message'])
                self.send_response_to_client(validate_admin_status,connection)
            elif(decoded_request['request_type']=='register_user'):
                create_user_status=self.create_and_populate_user_table(decoded_request['user_name'])
                self.send_response_to_client(create_user_status,connection)
            elif(decoded_request['request_type']=='create_topic'):
                create_topic_status=self.create_and_populate_topics_tables(decoded_request['topic_name'])
                self.send_response_to_client(create_topic_status,connection)                   
            elif(decoded_request['request_type']=='view_topic'):
                status,list_of_topics=self.get_topics()
                self.send_response_with_status_to_client(status,list_of_topics,connection)
            elif(decoded_request['request_type']=='read_msg'):
                status,list_of_messages=self.subscribe_topic(decoded_request['topic_name'],decoded_request['user_name'])
                self.send_response_with_status_to_client(status,list_of_messages,connection)        
            elif(decoded_request['request_type']=='send_msg'):
                status=self.create_and_populate_messages_tables(decoded_request['topic_name'],decoded_request['message'],decoded_request['user_name'])
                self.send_response_to_client(status,connection)
            
    def run(self,server_socket):
        server_socket.listen(5)
        print(server_listening,"{}:{}".format(HOST,PORT))
        while True:
            Client, address = server_socket.accept()
            print(connected_msg + address[0] + ':' + str(address[1]))
            start_new_thread(self.process_client_request, (Client,address))
            
            
    