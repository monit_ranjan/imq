import json
from protocol.protocol import ImqProtocol

class Parser:
    def get_encoded_json(header):
        header_dict={'version':header.protocol.version,'format':header.protocol.format,'status':header.status,'data':header.protocol.data}
        return json.dumps(json.JSONEncoder().encode(header_dict), indent=4)

    def get_decoded_json(header):
        return json.JSONDecoder().decode(json.loads(header))
