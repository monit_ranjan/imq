from protocol.protocol import ImqProtocol

class Request(ImqProtocol):
    def __init__(self,request_type,user_name='',topic_name='',message=''):
        self.protocol=ImqProtocol(user_name,topic_name,message)
        self.request_type = request_type
        