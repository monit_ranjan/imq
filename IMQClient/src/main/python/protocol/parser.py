import json
from protocol.protocol import ImqProtocol

class Parser:
    def get_encoded_json(header):
        header_dict={'version':header.protocol.version,'format':header.protocol.format,
                     'user_name':header.protocol.user_name,'topic_name':header.protocol.topic_name,
                     'message':header.protocol.message,'request_type':header.request_type}
        return json.dumps(json.JSONEncoder().encode(header_dict), indent=4)

    def get_decoded_json(header):
        return json.JSONDecoder().decode(json.loads(header))
