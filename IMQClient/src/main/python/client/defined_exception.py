class error(Exception):
    pass

class connection_refused_error(error):
    pass
    
class ConnectionRefusedError(connection_refused_error):
    pass