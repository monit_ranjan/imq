import sys
sys.path.append('../')
from protocol.request import Request
from string_literals import *

class Subscriber:
    
    def get_topics(self,client,client_socket):
        request=Request('view_topic')
        decoded_response= client.send_request(client_socket,request)
        if(decoded_response['status']==True):
            for topic in decoded_response['data']:
                print('{}. {}'.format(topic[0],topic[1]))
        else:
            print(decoded_response['data'])
            
    def get_messages_from_topic(self,client,client_socket,topic_name,user_name):
        request=Request('read_msg',topic_name=topic_name,user_name=user_name)
        decoded_response= client.send_request(client_socket,request)    
        if(decoded_response['status']):
            if (len(decoded_response['data'])>0):
                for message in decoded_response['data']:
                    if(message[1]=='-'):
                        print("{}".format(message[0]))
                    else:   
                        print("{} - {}".format(message[1],message[0]))
            else:
                print(no_msg)
        else:
            print(something_went_wrong)
    
    