import sys
import unittest
sys.path.append('../')
from client import Client
import warnings
import client
from unittest.mock import patch

class ClientTest(unittest.TestCase):
    
    def setUp(self):
        self.client_object = Client()
    
    def supress_resource_warning(self):
        warnings.filterwarnings(action="ignore",
                                message="unclosed",
                                category=ResourceWarning)
    
    def test_client_socket(self):
        self.supress_resource_warning()
        self.assertIsNotNone(self.client_object.client_socket())
        
    def test_create_connection(self):
        self.assertIsNotNone(self.client_object.create_connection())
    
    #Test for execution of commands before making connection to server
    def test_process_commands_1(self):
        input_values=['imq subscribe 1','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())
    
    #Test for proper syntax of commands after making connection to server
    def test_process_commands_2(self):
        input_values=['imq connect','IMQ login monit','imq register','imq Get topics','imqq create topic1','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())
    
    #Test for execution of commands before register after making connection to server
    def test_process_commands_3(self):
        input_values=['imq connect','imq subscribe 1','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())
    
    #Test for execution of commands after register before login after making connection  to server
    def test_process_commands_4(self):
        input_values=['imq connect','imq register test_user','imq subscribe 1','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())    
    
    #Test for creation of topic
    def test_process_commands_5(self):
        input_values=['imq connect','imq create test_topic','admin','admin','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())
    
    #Test for execution of commands after login 
    def test_process_commands_6(self):
        input_values=['imq connect','imq login test_user','imq subscribe 1','imq publish 1 -m "hello test"','imq disconnect']
        def mock_input(s=''):
            return input_values.pop(0)
        client.input = mock_input
        self.assertIsNone(self.client_object.process_commands())    
        
if __name__ == '__main__':
    unittest.main()
    
    
