from client import Client
from defined_exception import *
from string_literals import *

if __name__ == "__main__":
    try:
        client=Client()
        client.process_commands()
    except ConnectionRefusedError:
        print(connection_refused_error)
        