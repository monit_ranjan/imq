import sys
sys.path.append('../')
from constant import FORMAT, HOST, PORT
import socket,pickle
from string_literals import *
from protocol.request import Request
from protocol.parser import Parser
from subscriber import Subscriber
from publisher import Publisher

class Client:
    user_data={'userName':'','loginStatus':False}

    def client_socket(self):
        clientSocket = socket.socket()
        return clientSocket
    
    def send_request(self,client_socket,request):
        client_socket.send(str.encode(Parser.get_encoded_json(request)))
        response = client_socket.recv(1024)
        decoded_response=Parser.get_decoded_json(response.decode(FORMAT))
        return decoded_response
    
    def create_topic(self,client_socket,topic_name):
        request=Request('create_topic',topic_name=topic_name)
        return self.send_request(client_socket,request)
    
    def authenticate_admin(self,client_socket,user_name,password):
        request=Request('authenticate_admin',user_name=user_name,message=password)
        return self.send_request(client_socket,request)['status']
     
    def authenticate_and_create_topic(self,client_socket,topic_name):
        admin_user_name=input(enter_admin_user_name)
        admin_password=input(enter_admin_password)
        login_status=self.authenticate_admin(client_socket,admin_user_name,admin_password)
        if(login_status):            
            topic_creation_status=self.create_topic(client_socket,topic_name)
            if(topic_creation_status):
                print(topic_created)
            else:
                print(something_went_wrong)  
        else:
            print(invalid_admin)
        
    def validate_user_login(self,client_socket,user_name):
        Client.user_data['userName']=user_name
        request=Request('user_check',user_name=user_name)
        response=self.send_request(client_socket,request)    
        user_status=response['status']
        if(user_status==True):
            Client.user_data['loginStatus']=True
            print(login_success)
        else:
            print(register_user)
            
    def validate_user_input(self,user_command):
        if(user_command.count('"')==2 and user_command[-1]=='"' and user_command.split(' ')[4][0]=='"'):
            return True
        else: 
            return False
            
    def user_register(self,client_socket,user_name):
        Client.user_data['userName']=user_name
        request=Request('register_user',user_name=user_name)
        response=self.send_request(client_socket,request)    
        user_status=response['status']
        if(user_status==True):
            print(user_created_successfully)
        else:
            print(create_different_username)
    
    def create_connection(self):
        print(Wait_conn_msg)
        client_socket=self.client_socket()
        try:
            client_socket.connect((HOST, PORT))
            response = client_socket.recv(1024)
            print(response.decode(FORMAT))
            return client_socket
        except socket.error:
            print(unable_to_connect)
            
    def process_commands(self):
        client_socket=''
        print(imq_help)
        while True:
            print(enter_command)
            user_command=input().lstrip(' ').rstrip(' ')
            command_list=user_command.split(' ')
            if(user_command==imq_connect):
                client_socket=self.create_connection()
                print(topics_available)
                subscriber=Subscriber()
                subscriber.get_topics(self,client_socket)
            elif(user_command==imq_disconnect):
                print(thank_you)
                break
            elif(user_command==imq__help):
                print(command_help)
            elif (client_socket==''):
                print(connect_server)
            elif(user_command==imq_get_topics):
                print(topics_available)
                subscriber=Subscriber()
                subscriber.get_topics(self,client_socket)
            elif(" ".join(command_list[:2])==imq_login) and len(command_list)==3:
                self.validate_user_login(client_socket,command_list[2]) 
            elif(" ".join(command_list[:2])==imq_register)and len(command_list)==3:
                self.user_register(client_socket,command_list[2])
            elif(" ".join(command_list[:2])==imq_subscribe) and len(command_list)==3:
                if(Client.user_data['loginStatus']):
                    subscriber=Subscriber()
                    subscriber.get_messages_from_topic(self,client_socket,command_list[2],Client.user_data['userName'])
                else:    
                    print(can_not_subscribe) 
            elif(" ".join(command_list[:2])==imq_create)and len(command_list)==3:
                self.authenticate_and_create_topic(client_socket,command_list[2])
            elif(" ".join(command_list[:2])==imq_publish)and len(command_list)>=5 and command_list[3]=='-m':
                if(Client.user_data['loginStatus']):
                    command_validation_status=self.validate_user_input(user_command)
                    if(command_validation_status):
                        user_message=" ".join(command_list[4:])[1:-1]
                        publisher=Publisher()
                        publisher.publish_message(self,client_socket, command_list[2], user_message,Client.user_data['userName'])
                    else:
                        print(invalid_command)
                else:
                    print(can_not_publish)                                  
            else:
                print(no_command_exist)
               