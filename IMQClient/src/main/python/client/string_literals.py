command_help='''
**********
IMQ Help:
1.Login as user: imq login username
2.Create userid: imq register username
3.Get topics: imq get topics
4.Connect to Server : imq connect 
5.Publish Message: imq publish topicid -m "message"
6.Subscribe Message: imq subscribe topicid
7.Create Topic: imq create topicname
8.Disconnect : imq disconnect
**********
'''
Wait_conn_msg = "Waiting for connection"
unable_to_connect = "Unable to connect with server"
connection_refused_error = "server down"
input_msg='Send Message: '
no_msg="No messages found."
something_went_wrong="something went wrong.please contact admin"
msg_posted="Message posted successfully"
topic_created="topic created successfuly"
invalid_admin='Invalid admin credentials'
enter_admin_user_name='Please enter admin user name:'
enter_admin_password="Please enter admin password:"
login_success="Login Successful"
register_user="user does not exist. Please create your id."
user_created_successfully="user created Successfully.Please Login to continue"
create_different_username="username already exist. Please choose another username"
imq_help="Enter <imq help> to get command help"
enter_command="please enter command:"
topics_available="Topics Available:---"
connect_server="please connect to server first"
can_not_subscribe='cannot subscribe. Please login to continue.'
invalid_command="Please enter correct command to publish message"
can_not_publish='cannot publish. Please login to continue.'
no_command_exist="Command doesnot exist"
imq_connect='imq connect'
imq__help='imq help'
imq_get_topics='imq get topics'
imq_login='imq login'
imq_register='imq register'
imq_subscribe='imq subscribe'
imq_create='imq create'
imq_publish='imq publish'
imq_disconnect = 'imq disconnect'
thank_you = 'Thank you for using IMQ'
